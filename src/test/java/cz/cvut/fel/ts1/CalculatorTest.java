package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {
    private static Calculator calculator; //private and static pouze pro @BeforeAll

    @BeforeAll
    public static void setupAll(){
        System.out.println("Start testing...");
        calculator = new Calculator();
    }

    @BeforeEach //bude provedena před každou metodou označenou anotací @test
    public void setup(){
        System.out.println("Next operation...");
//        calculator = new Calculator();
    }
    @Test
    @Order(2)
    @DisplayName("Subtract {11} - {6} = {5}")
    public void subtrack_11minus6_return5(){
        System.out.println("Subtract {11} - {6} = {5}");
        int a = 11;
        int b = 6;
        int expected = 5;
        int result = calculator.subtract(a,b);
        Assertions.assertEquals(expected, result);
    }

    @Test
    @Order(1) //poradi testu
    @DisplayName("Sum {5} + {6} = {11}")
    public void add_5plus6_return11(){
        System.out.println("Sum {5} + {6} = {11}");
        //arange
//        Calculator calculator = new Calculator();
        int a = 5;
        int b = 6;
        int expected = 11;
        //act
        int result = calculator.add(a,b);
        //assert
        Assertions.assertEquals(expected, result);
    }
    @Test
    @Order(3)
    @DisplayName("Multiply {11} * {6} = {66}")
    public void multiply_11multiply6_return66(){
        System.out.println("Multiply {11} * {6} = {66}");
        int a = 11;
        int b = 6;
        int expected = 66;
        int result = calculator.multiply(a,b);
        Assertions.assertEquals(expected, result);
    }

    @Test
    @Order(4)
    @DisplayName("Divide {12} / {6} = {2}")
    public void divide_12divideBy6_return2() throws Exception {
        System.out.println("Divide {12} / {6} = {2}");
        int a = 12;
        int b = 6;
        int expected = 2;
        int result = calculator.divide(a,b);
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void throwException_divideByZero() {
        System.out.println("throwException divided By Zero");
//        final Calculator calculator = new Calculator();
        Assertions.assertThrows(Exception.class, () -> {
            calculator.divide(12, 0); //vola metodu
        });
    }
    @AfterAll
    public static void endOfTest() {
        System.out.println("Test is over.");
    }
}
